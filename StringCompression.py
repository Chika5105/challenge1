def compress(input_string):
    """Function accepts a string and returns a compressed version"""
    arr = [] #array stores compressed string contents. Used to optimize algoirthm performance
    compr_str = ''
    prev_char = ''
    count = 1 #counts non-interrupted repetitive characters in string
    if input_string == "":
        return ("Empty String Passed to Function!")
    #error check for wrong input to function
    if type(input_string) !=str:
        return ("Error! You passed an argument of type" +str( type(input_string)) +".", "Function only accepts strings")
    for char in input_string:
        if char != prev_char:  # If the previous and current characters don't match...
            if prev_char:  # then.. add the current count and character to our compressed string
                if count>1:
                    arr.append(prev_char + str(count))  #only assign count if there are more than 1 occurrences of a char
                else: arr.append(prev_char)  #just one occurrence, don't assign any count
            count = 1
            prev_char = char
        else:
            # Or increment our counter
            # if the characters do match
            count += 1 #still counting non-interrupted repetitive characters
    #continue till you reach end of string
    if count>1:
        arr.append(prev_char + str(count))
        compr_str = ''.join(arr)
        return compr_str
    else:
        arr.append(prev_char)
        compr_str = ''.join(arr)
        return compr_str 

#-------Some Test Cases-------------
print(compress("bbcceeee"))
print(compress("aaabbbcccaaa"))
print(compress("a"))
print(compress(10))
#----------------------------------Thorough Testing of the Above Algorithm with Unittest--------------

import unittest
class StringCompressionTest(unittest.TestCase):
    def test_compress_empty_string(self):
        self.assertMultiLineEqual(compress(''), 'Empty String Passed to Function!')

    def test_compress_single_characters_only_are_compressed_without_count(self):
        self.assertMultiLineEqual(compress('abc'), 'abc')

    def test_compress_string_with_no_single_characters(self):
        self.assertMultiLineEqual(compress('aabbbcccc'), 'a2b3c4')

    def test_compress_single_characters_mixed_with_repeated_characters(self):
        self.assertMultiLineEqual(
            compress('wwwwwwwwwwwwbwwwwwwwwwwwwbbbwwwwwwwwwwwwwwwwwwwwwwwwb'),
            'w12bw12b3w24b')

    def test_compress_multiple_whitespace_mixed_in_string(self):
         #special test case. Function counts whitespaces (leading, trailing and inbetween)
        self.assertMultiLineEqual(compress('  abcc cdd  '), ' 2abc2 cd2 2')

    def test_compress_lowercase_characters(self):
        self.assertMultiLineEqual(compress('aabbbcccc'), 'a2b3c4')
    def test_compress_string_with_single_character(self):
        self.assertMultiLineEqual(compress('a'), 'a')

   


if __name__ == '__main__':
    unittest.main()
    


#----------------------------Time Complexity Explanation--------------------------------------
""" The time complexity of the algorithm is O(n)-worst case. 
There are n calls to append (as per the for loop)and each call runs in O(1) amortized. 
Again, the return statement runs in O(1).
Hence, the total number of calls will run in O(n) time. 
The final join runs in O(1) time in Python. Thus the algorithm altogether runs in linear time 
proportional to the length of the input_string. The created array arr is necessary because it optimizes
the performance of this algorithm. If I had used a string concatenation method like comp_str += prev_char + count
then this algorithm would have run in quadratic time O(n^2). This is because strings are immutable in Python and 
each call to comp_str += prev_char + count would have created a new string instance and reassigned comp_str. 
Creating a new string instance takes O(n) time in Python and together with the for loop would have taken 
O(n*n) time which would would have taken O(n^2) running time
"""