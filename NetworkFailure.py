"""
Created on Sat May  2 18:56:20 2020

@author: jinanwachikafavour
"""

    
class Graph:
    """Class that represents a simple graph using an adjacency map data structure
    Here, vertices are stored as keys while edges are stored as dictionary values
    reference: https://algorithms.tutorialhorizon.com/implement-graph-using-map-java/
    """
    
  #------------------------- nested Vertex class -------------------------
    class Vertex:
        """ Class implements a lightweight vertex graph structure"""
        __slots__ = '_element'
        
        def __init__(self,x):
            """Use Graph's insert_verte(x) method, Don't call this constructor directly"""
            self._element=x
            
        def element(self):
            """Returns element associated with this vertex"""
            return self._element
        
        def __hash__(self):
            return hash(id(self)) #allows vertex to be a map/set's key
        def __str__(self): #customized string output
            return str(self._element)
    
    
    
  #------------------------- nested Edge class -------------------------
    class Edge:
        """Class implements a lightweight structure for edge graph structure"""
        __slots__ = '_origin','_destination', '_element'
        
        def __init__(self, u,v,x):
            """Use Graph's insert_edge(u,v,x) method, Don't call this constructor directly"""
            self._origin = u #starting node
            self._destination = v #ending node
            self._element = x
            
        def endpoints(self):
            """Returns (u,v) tuple for vertices u and v."""
            return (self._origin, self._destination)
        
        def opposite(self,v):
            """Returns the vertex that is opposite v on this edge."""
            if not isinstance(v, Graph.Vertex):
                raise TypeError('v must be a Vertex')
            return self._destination if v is self._origin else self._origin
            raise ValueError('V is not incident to this edge')
        
        def element(self):
            """Returns element associated with this edge."""
            return self._element
        
        def __hash__(self):
            return hash((self._origin, self._destination)) #allows edge to be a map/set key
        def __str__(self): #customized string output
            return '({0},{1},{2})'.format(self._origin,self._destination,self._element)
        
    
    
  #------------------------- Graph methods -------------------------
    def __init__(self, directed=False):
        """Creates an empty graph that is undirected by default
        Set directed parameter to True if graph is directed
        """
        self._outgoing = {}
        #only creates second map for a directed graph, uses an alias for an undirected graph
        self._incoming = {} if directed else self._outgoing
        
    def _validate_vertex(self, v):
        """Verifies that v is a Vertex of this graph."""
        if not isinstance(v, self.Vertex):
          raise TypeError('Vertex expected!')
        if v not in self._outgoing:
          raise ValueError('This vertex does not belong to this graph.')
        
    def is_directed(self):
        """Returns True if graph is directed or False if undirected
        The return value is based on the original declaration of the graph 
        and not its contents"""
        
        return self._incoming is not self._outgoing #it's a directed graph if the maps are distinct
    
    def vertex_count(self):
        """Returns the number of vertices in the graph."""
        return len(self._outgoing)
    
    def vertices(self):
        """Returns an iteration of all vertices of the graph"""
        return self._outgoing.keys() #stores vertices as keys in dictionary
    
    def edge_count(self):
        """Returns the number of edges in the graph"""
        total = sum(len(self._outgoing[v]) for v in self._outgoing)
        return total if self.is_directed() else total//2 #don't double count edges for undirected graphs
    
    def edges(self):
        """Returns a set of all edges in the graph"""
        result = set() #unique elements to avoid double-reporting edges for an undirected graph
        for secondary_map in self._outgoing.values():
            result.update(secondary_map.values()) #add edges to resulting set
        return result
    
    def get_edge(self,u,v):
        """Returns the edge from u to v, or None if not adjacent"""
        self._validate_vertex(u)
        self._validate_vertex(v)
        return self._outgoing[u].get(v) #returns None if v is not adjacent
    
    def degree(self,v,outgoing=True):
        """Returns number of outgoing edges incident to vertex v in the graph.
        If graph is directed, optional parameter counts incoming edges"""
        
        self._validate_vertex(v)
        adj = self._outgoing if outgoing else self._incoming 
        #call this method twice for directed graphs with outgoing set to True for first call and False 
        #for second. Sum their results to get degrees for the vertex in the directed graph
        return len(adj[v])
    
    def incident_edges(self,v,outgoing = True):
        """Return all outgoing edges incident to vertex v in the graph.
        If graph is directed, optional parameter requests incoming edges"""
        
        self._validate_vertex(v)
        adj = self._outgoing if outgoing else self._incoming
        for edge in adj[v].values():
            yield edge
            
    def insert_vertex(self, x = None):
        """Inserts and returns a new Vertex with element x."""
        v = self.Vertex(x)
        self._outgoing[v] = {}
        if self.is_directed():
            self._incoming[v] = {} #distinct map for incoming edges for directed graphs
        return v
    def insert_edge(self,u,v,x=None):
        """Inserts and returns a new Edge from u to v with 
        auxiliary element x."""
        if self.get_edge(u, v) is not None:  # error checking for u and v not part of graph and if both u and v are already adjacent
            raise ValueError('u and v are already adjacent')
        e = self.Edge(u,v,x)
        self._outgoing[u][v] = e
        self._incoming[v][u] = e

    def __str__(self):
        """Custom string output for graph class"""
        response = "nodes: "
        for k in self.vertices():
            response += str(k) + " "
        response+= "\nedges: "
        for edge in self.edges():
            response+=str(edge) + " "
        return response 

        
        


 
def graph_from_edgelist(tuple_input, directed=False):
  """Make a graph instance based on a sequence of edge tuples.
  Edges can be either of from (origin,destination) or
  (origin,destination,element). Vertex set is presumed to be those
  incident to at least one edge.
  vertex labels are assumed to be hashable.
  """
  graph_object = Graph(directed) 
  V = set() #ensures nodes are unique per instruction
  for e in tuple_input:
    V.add(e[0])
    V.add(e[1])

  verts = {}  # map from vertex label to Vertex instance
  for v in V:
    verts[v] = graph_object.insert_vertex(v) #add vertex to newly created object
  for e in tuple_input:
    src = e[0]
    dest = e[1]
    element = e[2] if len(e) > 2 else None
    graph_object.insert_edge(verts[src],verts[dest],element)

  return graph_object


def identify_router(graph):
    """Function returns the router (node)
    with the highest number of connections (degrees)"""
    vertex_connection_dict = {} #stores vertices as keys with their respective degrees as values
    for vertex in graph.vertices(): 
        vertex_degree = graph.degree(vertex, outgoing = True) + graph.degree(vertex, outgoing = False) 
        #For this problem, all graphs are directed, so total is given by expression above
        vertex_connection_dict[vertex] = vertex_degree  #assign vertext as key to  vertex_degree as values
    largest_connections = 0 #initiliaze largest connection as 0
    for key , value in vertex_connection_dict.items():
        if value>largest_connections: 
            #this value in current iteration > largest_connections
            largest_connections = value #reassign to current_value
            expected_node = key #vertex label for current node with largest_connections
    return "The node with the highest connections is Router " + str(expected_node)

#--------------------TestCases-----------------------------------

#Test 1 => 1 -> 2 -> 3 -> 5 -> 2 -> 1 = 2 

test1 = (
    ('1','2'), ('2','3'), ('2','1'), ('3','5'),
    ('5','2'))


#Test 2 => 1 -> 3 -> 5 -> 6 -> 4 -> 5 -> 2 -> 6
test2 = (
     ('1','3'), ('3', '5'), ('5', '6'), ('6', '4'), ('4', '5'), ('5', '2'), ('2', '6'))

#Test 3 => 2 -> 4 -> 6 -> 2 -> 5 -> 6 = 2
test3 = (
        ('3','2'), ('2', '4'), ('4', '6'), ('6', '2'), ('2', '5'), ('5', '6'))



print(identify_router(graph_from_edgelist(test1, True)))
print(identify_router(graph_from_edgelist(test2, True)))
print(identify_router(graph_from_edgelist(test3, True)))

#------------------------Time Complexity Explanation---------------------------------------
""" 
The algorithm runs in O(n^2) complexity-worst case
The first for loop runs in O(n) time because it visits all the vertices. 
The graph.vertices() method runs in O(n) time because it iterates through all keys in the dictionary-check method definition
The graph.degree() method runs in O(1) time since len(dictionary) runs in constant time in Python
Assignment operation also runs in constant time. 
Hence,the entire first loop runs in O(n^2) because for each vertex, the .vertices method also runs in linear time
The second for loop runs in O(n) time because each run of the loop iterates through both keys and values
in the dic, performs an assignment operation and a possible reassignment operation (both constant time).
Summing these operations, we have a worst-case running time algorithm of O(n^2)

"""